/*
 *  libdvbv5 — a Rust binding to the libdvbv5 library from V4L2.
 *
 *  Copyright © 2020  Russel Winder
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

//! Various enumerations that are not part of the underlying libdvbv5.

#![allow(non_camel_case_types)]

use dvbv5_sys;

/// Properties that can be retrieved.
///
/// These are simply macros values in libdvbv5.
#[derive(Clone, Copy, Eq, PartialEq)]
#[repr(u32)]
pub enum dtv_retrievable_properties {
    // Commands
    DTV_UNDEFINED = dvbv5_sys::DTV_UNDEFINED,
    DTV_TUNE = dvbv5_sys::DTV_TUNE,
    DTV_CLEAR = dvbv5_sys::DTV_CLEAR,
    DTV_FREQUENCY = dvbv5_sys::DTV_FREQUENCY,
    DTV_MODULATION = dvbv5_sys::DTV_MODULATION,
    DTV_BANDWIDTH_HZ = dvbv5_sys::DTV_BANDWIDTH_HZ,
    DTV_INVERSION = dvbv5_sys::DTV_INVERSION,
    DTV_DISEQC_MASTER = dvbv5_sys::DTV_DISEQC_MASTER,
    DTV_SYMBOL_RATE = dvbv5_sys::DTV_SYMBOL_RATE,
    DTV_INNER_FEC = dvbv5_sys::DTV_INNER_FEC,
    DTV_VOLTAGE = dvbv5_sys::DTV_VOLTAGE,
    DTV_TONE = dvbv5_sys::DTV_TONE,
    DTV_PILOT = dvbv5_sys::DTV_PILOT,
    DTV_ROLLOFF = dvbv5_sys::DTV_ROLLOFF,
    DTV_DISEQC_SLAVE_REPLY = dvbv5_sys::DTV_DISEQC_SLAVE_REPLY,
    DTV_FE_CAPABILITY_COUNT = dvbv5_sys::DTV_FE_CAPABILITY_COUNT,
    DTV_FE_CAPABILITY = dvbv5_sys::DTV_FE_CAPABILITY,
    DTV_DELIVERY_SYSTEM = dvbv5_sys::DTV_DELIVERY_SYSTEM,
    DTV_ISDBT_PARTIAL_RECEPTION = dvbv5_sys::DTV_ISDBT_PARTIAL_RECEPTION,
    DTV_ISDBT_SOUND_BROADCASTING = dvbv5_sys::DTV_ISDBT_SOUND_BROADCASTING,
    DTV_ISDBT_SB_SUBCHANNEL_ID = dvbv5_sys::DTV_ISDBT_SB_SUBCHANNEL_ID,
    DTV_ISDBT_SB_SEGMENT_IDX = dvbv5_sys::DTV_ISDBT_SB_SEGMENT_IDX,
    DTV_ISDBT_SB_SEGMENT_COUNT = dvbv5_sys::DTV_ISDBT_SB_SEGMENT_COUNT,
    DTV_ISDBT_LAYERA_FEC = dvbv5_sys::DTV_ISDBT_LAYERA_FEC,
    DTV_ISDBT_LAYERA_MODULATION = dvbv5_sys::DTV_ISDBT_LAYERA_MODULATION,
    DTV_ISDBT_LAYERA_SEGMENT_COUNT = dvbv5_sys::DTV_ISDBT_LAYERA_SEGMENT_COUNT,
    DTV_ISDBT_LAYERA_TIME_INTERLEAVING = dvbv5_sys::DTV_ISDBT_LAYERA_TIME_INTERLEAVING,
    DTV_ISDBT_LAYERB_FEC = dvbv5_sys::DTV_ISDBT_LAYERB_FEC,
    DTV_ISDBT_LAYERB_MODULATION = dvbv5_sys::DTV_ISDBT_LAYERB_MODULATION,
    DTV_ISDBT_LAYERB_SEGMENT_COUNT = dvbv5_sys::DTV_ISDBT_LAYERB_SEGMENT_COUNT,
    DTV_ISDBT_LAYERB_TIME_INTERLEAVING = dvbv5_sys::DTV_ISDBT_LAYERB_TIME_INTERLEAVING,
    DTV_ISDBT_LAYERC_FEC = dvbv5_sys::DTV_ISDBT_LAYERC_FEC,
    DTV_ISDBT_LAYERC_MODULATION = dvbv5_sys::DTV_ISDBT_LAYERC_MODULATION,
    DTV_ISDBT_LAYERC_SEGMENT_COUNT = dvbv5_sys::DTV_ISDBT_LAYERC_SEGMENT_COUNT,
    DTV_ISDBT_LAYERC_TIME_INTERLEAVING = dvbv5_sys::DTV_ISDBT_LAYERC_TIME_INTERLEAVING,
    DTV_API_VERSION = dvbv5_sys::DTV_API_VERSION,
    DTV_CODE_RATE_HP = dvbv5_sys::DTV_CODE_RATE_HP,
    DTV_CODE_RATE_LP = dvbv5_sys::DTV_CODE_RATE_LP,
    DTV_GUARD_INTERVAL = dvbv5_sys::DTV_GUARD_INTERVAL,
    DTV_TRANSMISSION_MODE = dvbv5_sys::DTV_TRANSMISSION_MODE,
    DTV_HIERARCHY = dvbv5_sys::DTV_HIERARCHY,
    DTV_ISDBT_LAYER_ENABLED = dvbv5_sys::DTV_ISDBT_LAYER_ENABLED,
    DTV_STREAM_ID = dvbv5_sys::DTV_STREAM_ID,
    // DTV_ISDBS_TS_ID_LEGACY = dvbv5_sys::DTV_ISDBS_TS_ID_LEGACY, // Repeat of use of 42.
    DTV_DVBT2_PLP_ID_LEGACY = dvbv5_sys::DTV_DVBT2_PLP_ID_LEGACY,
    DTV_ENUM_DELSYS = dvbv5_sys::DTV_ENUM_DELSYS,
    DTV_ATSCMH_FIC_VER = dvbv5_sys::DTV_ATSCMH_FIC_VER,
    DTV_ATSCMH_PARADE_ID = dvbv5_sys::DTV_ATSCMH_PARADE_ID,
    DTV_ATSCMH_NOG = dvbv5_sys::DTV_ATSCMH_NOG,
    DTV_ATSCMH_TNOG = dvbv5_sys::DTV_ATSCMH_TNOG,
    DTV_ATSCMH_SGN = dvbv5_sys::DTV_ATSCMH_SGN,
    DTV_ATSCMH_PRC = dvbv5_sys::DTV_ATSCMH_PRC,
    DTV_ATSCMH_RS_FRAME_MODE = dvbv5_sys::DTV_ATSCMH_RS_FRAME_MODE,
    DTV_ATSCMH_RS_FRAME_ENSEMBLE = dvbv5_sys::DTV_ATSCMH_RS_FRAME_ENSEMBLE,
    DTV_ATSCMH_RS_CODE_MODE_PRI = dvbv5_sys::DTV_ATSCMH_RS_CODE_MODE_PRI,
    DTV_ATSCMH_RS_CODE_MODE_SEC = dvbv5_sys::DTV_ATSCMH_RS_CODE_MODE_SEC,
    DTV_ATSCMH_SCCC_BLOCK_MODE = dvbv5_sys::DTV_ATSCMH_SCCC_BLOCK_MODE,
    DTV_ATSCMH_SCCC_CODE_MODE_A = dvbv5_sys::DTV_ATSCMH_SCCC_CODE_MODE_A,
    DTV_ATSCMH_SCCC_CODE_MODE_B = dvbv5_sys::DTV_ATSCMH_SCCC_CODE_MODE_B,
    DTV_ATSCMH_SCCC_CODE_MODE_C = dvbv5_sys::DTV_ATSCMH_SCCC_CODE_MODE_C,
    DTV_ATSCMH_SCCC_CODE_MODE_D = dvbv5_sys::DTV_ATSCMH_SCCC_CODE_MODE_D,
    DTV_INTERLEAVING = dvbv5_sys::DTV_INTERLEAVING,
    DTV_LNA = dvbv5_sys::DTV_LNA,
    DTV_STAT_SIGNAL_STRENGTH = dvbv5_sys::DTV_STAT_SIGNAL_STRENGTH,
    DTV_STAT_CNR = dvbv5_sys::DTV_STAT_CNR,
    DTV_STAT_PRE_ERROR_BIT_COUNT = dvbv5_sys::DTV_STAT_PRE_ERROR_BIT_COUNT,
    DTV_STAT_PRE_TOTAL_BIT_COUNT = dvbv5_sys::DTV_STAT_PRE_TOTAL_BIT_COUNT,
    DTV_STAT_POST_ERROR_BIT_COUNT = dvbv5_sys::DTV_STAT_POST_ERROR_BIT_COUNT,
    DTV_STAT_POST_TOTAL_BIT_COUNT = dvbv5_sys::DTV_STAT_POST_TOTAL_BIT_COUNT,
    DTV_STAT_ERROR_BLOCK_COUNT = dvbv5_sys::DTV_STAT_ERROR_BLOCK_COUNT,
    DTV_STAT_TOTAL_BLOCK_COUNT = dvbv5_sys::DTV_STAT_TOTAL_BLOCK_COUNT,
    DTV_SCRAMBLING_SEQUENCE_INDEX = dvbv5_sys::DTV_SCRAMBLING_SEQUENCE_INDEX,
    // User commands.
    DTV_POLARIZATION = dvbv5_sys::DTV_POLARIZATION,
    DTV_VIDEO_PID = dvbv5_sys::DTV_VIDEO_PID,
    DTV_AUDIO_PID = dvbv5_sys::DTV_AUDIO_PID,
    DTV_SERVICE_ID = dvbv5_sys::DTV_SERVICE_ID,
    DTV_CH_NAME = dvbv5_sys::DTV_CH_NAME,
    DTV_VCHANNEL = dvbv5_sys::DTV_VCHANNEL,
    DTV_SAT_NUMBER = dvbv5_sys::DTV_SAT_NUMBER,
    DTV_DISEQC_WAIT = dvbv5_sys::DTV_DISEQC_WAIT,
    DTV_DISEQC_LNB = dvbv5_sys::DTV_DISEQC_LNB,
    DTV_FREQ_BPF = dvbv5_sys::DTV_FREQ_BPF,
    DTV_PLS_CODE = dvbv5_sys::DTV_PLS_CODE,
    DTV_PLS_MODE = dvbv5_sys::DTV_PLS_MODE,
    DTV_COUNTRY_CODE = dvbv5_sys::DTV_COUNTRY_CODE,
    // Status Commands
    DTV_STATUS = dvbv5_sys::DTV_STATUS,
    DTV_BER = dvbv5_sys::DTV_BER,
    DTV_PER = dvbv5_sys::DTV_PER,
    DTV_QUALITY = dvbv5_sys::DTV_QUALITY,
    DTV_PRE_BER = dvbv5_sys::DTV_PRE_BER,
}

/// The log levels.
#[derive(Clone, Copy, Eq, PartialEq)]
#[repr(u32)]
pub enum log_level {
    LOG_EMERG = dvbv5_sys::LOG_EMERG,
    LOG_ALERT = dvbv5_sys::LOG_ALERT,
    LOG_CRIT = dvbv5_sys::LOG_CRIT,
    LOG_ERR = dvbv5_sys::LOG_ERR,
    LOG_WARNING = dvbv5_sys::LOG_WARNING,
    LOG_NOTICE = dvbv5_sys::LOG_NOTICE,
    LOG_INFO = dvbv5_sys::LOG_INFO,
    LOG_DEBUG = dvbv5_sys::LOG_DEBUG,
    // LOG_PRIMASK = dvbv5_sys::LOG_PRIMASK, // Repeat of 7.
    // LOG_KERN = dvbv5_sys::LOG_KERN, // Repeat of 0.
    LOG_USER = dvbv5_sys::LOG_USER,
    // LOG_MAIL = dvbv5_sys::LOG_MAIL, // Repeat use of 8.
    LOG_DAEMON = dvbv5_sys::LOG_DAEMON,
    LOG_AUTH = dvbv5_sys::LOG_AUTH,
    LOG_SYSLOG = dvbv5_sys::LOG_SYSLOG,
    LOG_LPR = dvbv5_sys::LOG_LPR,
    LOG_NEWS = dvbv5_sys::LOG_NEWS,
    LOG_UUCP = dvbv5_sys::LOG_UUCP,
    LOG_CRON = dvbv5_sys::LOG_CRON,
    LOG_AUTHPRIV = dvbv5_sys::LOG_AUTHPRIV,
    LOG_FTP = dvbv5_sys::LOG_FTP,
    LOG_LOCAL0 = dvbv5_sys::LOG_LOCAL0,
    LOG_LOCAL1 = dvbv5_sys::LOG_LOCAL1,
    LOG_LOCAL2 = dvbv5_sys::LOG_LOCAL2,
    LOG_LOCAL3 = dvbv5_sys::LOG_LOCAL3,
    LOG_LOCAL4 = dvbv5_sys::LOG_LOCAL4,
    LOG_LOCAL5 = dvbv5_sys::LOG_LOCAL5,
    LOG_LOCAL6 = dvbv5_sys::LOG_LOCAL6,
    LOG_LOCAL7 = dvbv5_sys::LOG_LOCAL7,
    // LOG_NFACILITIES = dvbv5_sys::LOG_NFACILITIES, // Repeat use of 24.
    LOG_FACMASK = dvbv5_sys::LOG_FACMASK,
    // LOG_PID = dvbv5_sys::LOG_PID, // Repeat use of 1
    // LOG_CONS = dvbv5_sys::LOG_CONS, // Repeat use of 2.
    // LOG_ODELAY = dvbv5_sys::LOG_ODELAY, // Repeat use of 4.
    // LOG_NDELAY = dvbv5_sys::LOG_NDELAY, // Repeat use of 8.
    LOG_NOWAIT = dvbv5_sys::LOG_NOWAIT,
    // LOG_PERROR = dvbv5_sys::LOG_PERROR, // Repeat use of 32.
}
